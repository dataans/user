FROM eu.gcr.io/dataans/user_builder:4 as builder

COPY ./Cargo.toml ./Cargo.toml
RUN rm src/*.rs

ADD src ./src
ADD deploy ./deploy

RUN rm ./target/release/deps/user*
RUN cargo build --release

FROM frolvlad/alpine-glibc:glibc-2.29
ARG APP=/usr/src/app

EXPOSE 8000

COPY --from=builder /user/target/release/user ./user

CMD ["./user"]
