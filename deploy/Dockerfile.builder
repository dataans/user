FROM rust:latest

RUN USER=root cargo new --bin user

WORKDIR ./user

COPY ./Cargo.toml ./Cargo.toml

RUN cargo build --release
