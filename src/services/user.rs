use std::sync::Arc;

use actix_multipart::{Field, MultipartError};
use actix_web::{http::StatusCode, ResponseError};
use futures_util::TryStreamExt;
use image::{storage::cloud_storage::CloudStorageUtils, transform_file_name, ImageError};
use session_manager::SessionError;
use thiserror::Error;
use uuid::Uuid;

use crate::{
    api::types::{UpdateUserAccountDataRequest, UserResponse},
    config::{AVATARS_BUCKET_ENV, AVATARS_HOST},
    db::{user::UserRepository, DbError},
    model::User,
};

use super::email::{EmailError, EmailService};

// maximal avatars file size in bytes: 1Mb
const MAX_AVATAR_FILE_SIZE: usize = 1048576;

#[derive(Error, Debug)]
pub enum UserError {
    #[error("Internal server error: {0:?}")]
    InternalDbError(#[from] DbError),
    #[error("Authentication error: {0}")]
    NotAuthorized(String),
    #[error("Internal server error: {0}")]
    Internal(String),
    #[error("User with id {0:?} not found")]
    NotFound(Uuid),
    #[error("Email error: {0:?}")]
    Email(#[from] EmailError),
    #[error("{0:?}")]
    Multipart(#[from] MultipartError),
    #[error("Form error: {0:?}")]
    Form(String),
    #[error("Image error: {0:?}")]
    Image(#[from] ImageError),
}

impl From<SessionError> for UserError {
    fn from(e: SessionError) -> Self {
        match e {
            SessionError::SessionToken(error) => UserError::NotAuthorized(error),
            SessionError::Crypto(error) => UserError::Internal(error),
            SessionError::RedisConnection(error) => UserError::Internal(format!("{}", error)),
        }
    }
}

impl ResponseError for UserError {
    fn status_code(&self) -> StatusCode {
        match self {
            UserError::NotAuthorized(_) => StatusCode::UNAUTHORIZED,
            UserError::Form(_) => StatusCode::BAD_REQUEST,
            UserError::Multipart(_) => StatusCode::BAD_REQUEST,
            UserError::Email(email_error) => email_error.status_code(),
            e => {
                log::error!("{:?}", e);
                StatusCode::INTERNAL_SERVER_ERROR
            }
        }
    }
}

pub struct UserService {
    user_repository: UserRepository,
    email_service: Arc<EmailService>,
    storage_utils: CloudStorageUtils,
}

impl UserService {
    pub fn new(
        user_repository: UserRepository,
        email_service: Arc<EmailService>,
        storage_utils: CloudStorageUtils,
    ) -> Self {
        Self {
            user_repository,
            email_service,
            storage_utils,
        }
    }

    pub async fn user_data(&self, user_id: &Uuid) -> Result<UserResponse, UserError> {
        let User {
            id,
            username,
            full_name,
            avatar_url,
            joined_at,
            primary_email,
            two_fa_secret: _,
            is_2fa_enabled,
        } = self
            .user_repository
            .find_by_id(user_id)
            .await?
            .ok_or(UserError::NotFound(*user_id))?;

        let emails = self
            .email_service
            .get_user_emails(user_id)
            .await?
            .into_iter()
            .map(|email| email.email)
            .collect();

        Ok(UserResponse {
            id,
            username,
            full_name,
            emails,
            primary_email,
            avatar_url,
            joined_at,
            is_2fa_enabled,
        })
    }

    pub async fn users_data(&self, users_ids: &[Uuid]) -> Result<Vec<UserResponse>, UserError> {
        let users = self.user_repository.find_by_ids(users_ids).await?;

        let mut response_users = Vec::with_capacity(users.len());

        for User {
            id,
            username,
            full_name,
            avatar_url,
            joined_at,
            primary_email,
            two_fa_secret: _,
            is_2fa_enabled,
        } in users
        {
            let emails = self
                .email_service
                .get_user_emails(&id)
                .await?
                .into_iter()
                .map(|email| email.email)
                .collect();

            response_users.push(UserResponse {
                id,
                username,
                full_name,
                emails,
                primary_email,
                avatar_url,
                joined_at,
                is_2fa_enabled,
            })
        }

        Ok(response_users)
    }

    pub async fn update_user_account_data(
        &self,
        data: UpdateUserAccountDataRequest,
        user_id: &Uuid,
    ) -> Result<UserResponse, UserError> {
        let mut user = self
            .user_repository
            .find_by_id(user_id)
            .await?
            .ok_or(UserError::NotFound(*user_id))?;

        let UpdateUserAccountDataRequest {
            username,
            full_name,
            avatar_url,
        } = data;

        if let Some(username) = username {
            user.username = username;
        }

        if let Some(full_name) = full_name {
            user.full_name = full_name;
        }

        if let Some(avatar_url) = avatar_url {
            user.avatar_url = avatar_url;
        }

        self.user_repository.update_user_account_data(&user).await?;

        self.user_data(user_id).await
    }

    pub async fn change_avatar(
        &self,
        mut avatar_file_field: Field,
        user_id: &Uuid,
    ) -> Result<UserResponse, UserError> {
        let mut user = self
            .user_repository
            .find_by_id(user_id)
            .await?
            .ok_or(UserError::NotFound(*user_id))?;

        let info = avatar_file_field.content_disposition();

        let filename = info
            .get_filename()
            .map(transform_file_name)
            .unwrap_or_else(|| transform_file_name(&Uuid::new_v4().to_string()));

        let file_type = avatar_file_field.content_type().type_().as_str();

        if file_type != "image" {
            return Err(UserError::Form(format!(
                "attached file is not a image. got: {}",
                file_type
            )));
        }

        let mut raw_image_data = Vec::new();

        while let Some(chunk) = avatar_file_field.try_next().await? {
            raw_image_data.extend_from_slice(&chunk);

            if raw_image_data.len() > MAX_AVATAR_FILE_SIZE {
                return Err(UserError::Form(
                    "avatar image is too large. maximum size is 1Mb".into(),
                ));
            }
        }

        let content_type = avatar_file_field.content_type().essence_str();

        self.storage_utils
            .save_raw_image(
                raw_image_data,
                &std::env::var(AVATARS_BUCKET_ENV).unwrap(),
                &filename,
                content_type,
            )
            .await?;

        user.avatar_url = format!("{}/{}", AVATARS_HOST, filename);

        self.user_repository.update_user_account_data(&user).await?;

        self.user_data(user_id).await
    }
}
