use std::sync::Arc;

use actix_web::{http::StatusCode, ResponseError};
use thiserror::Error;
use uuid::Uuid;

use crate::{
    api::types::EmailResponse,
    db::{email::EmailRepository, DbError},
};

#[derive(Error, Debug)]
pub enum EmailError {
    #[error("Internal server error: {0:?}")]
    InternalDbError(#[from] DbError),
    #[error("Authentication error: {0}")]
    NotAuthorized(String),
    #[error("Internal server error: {0}")]
    Internal(String),
    #[error("User with id {0:?} not found")]
    NotFound(Uuid),
}

impl ResponseError for EmailError {
    fn status_code(&self) -> StatusCode {
        match self {
            EmailError::NotAuthorized(_) => StatusCode::UNAUTHORIZED,
            EmailError::NotFound(_) => StatusCode::NOT_FOUND,
            _ => StatusCode::INTERNAL_SERVER_ERROR,
        }
    }
}

pub struct EmailService {
    email_repository: Arc<EmailRepository>,
}

impl EmailService {
    pub fn new(email_repository: Arc<EmailRepository>) -> Self {
        Self { email_repository }
    }

    pub async fn get_user_emails(&self, user_id: &Uuid) -> Result<Vec<EmailResponse>, EmailError> {
        Ok(self
            .email_repository
            .find_user_emails(user_id)
            .await?
            .into_iter()
            .map(|email| email.into())
            .collect())
    }
}
