use std::sync::Arc;

use actix_cors::Cors;
use actix_web::{web, App, HttpServer};
use async_mutex::Mutex;
use deadpool_postgres::{tokio_postgres::NoTls, Runtime};
use image::storage::cloud_storage::CloudStorageUtils;
use log::error;
use session_manager::SessionService;
use tonic::transport::Server;

use crate::{
    api::handlers::{
        default_route, health, root_health, update_user_account_data, upload_image, user_data,
    },
    config::{bind_address, check_env_vars, grpc_bind_address, pool_config},
    db::{email::EmailRepository, user::UserRepository},
    generated::user::user_service_server::UserServiceServer,
    grpc::UserGrpcServer,
    logging::setup_logger,
    services::{email::EmailService, user::UserService},
};

pub struct AppData {
    pub user_service: Mutex<UserService>,
    pub session_service: Mutex<SessionService>,
}

impl AppData {
    pub fn new() -> Self {
        let pool = Arc::new(Mutex::new(
            pool_config()
                .create_pool(Some(Runtime::Tokio1), NoTls)
                .unwrap(),
        ));

        let email_service = Arc::new(EmailService::new(Arc::new(EmailRepository::new(
            pool.clone(),
        ))));

        Self {
            user_service: Mutex::new(UserService::new(
                UserRepository::new(pool),
                email_service,
                CloudStorageUtils::new(),
            )),
            session_service: Mutex::new(SessionService::new_from_env()),
        }
    }
}

impl Default for AppData {
    fn default() -> Self {
        Self::new()
    }
}

#[allow(deprecated)]
pub async fn start_app() -> Vec<tokio::task::JoinHandle<Result<(), ()>>> {
    check_env_vars();
    setup_logger();

    let http_server = HttpServer::new(move || {
        App::new()
            .wrap(actix_web::middleware::Logger::default())
            .wrap(
                Cors::default()
                    .expose_any_header()
                    .supports_credentials()
                    .allow_any_header()
                    .allow_any_origin()
                    .allow_any_method(),
            )
            .default_service(actix_web::web::route().to(default_route))
            .data(AppData::new())
            .service(root_health)
            .service(health)
            .service(
                web::scope("/api/v1/user")
                    .service(user_data)
                    .service(update_user_account_data)
                    .service(upload_image),
            )
    })
    .bind(bind_address())
    .unwrap()
    .run();

    let http_server = tokio::spawn(async move {
        match http_server.await {
            Ok(_) => {}
            Err(err) => error!("http server error: {:?}", err),
        };

        Ok(())
    });

    let grpc_server = tokio::spawn(async move {
        let pool = pool_config()
            .create_pool(Some(Runtime::Tokio1), NoTls)
            .unwrap();
        match Server::builder()
            .add_service(UserServiceServer::new(UserGrpcServer::new(Arc::new(
                UserService::new(
                    UserRepository::new(Arc::new(Mutex::new(pool.clone()))),
                    Arc::new(EmailService::new(Arc::new(EmailRepository::new(Arc::new(
                        Mutex::new(pool),
                    ))))),
                    CloudStorageUtils::new(),
                ),
            ))))
            .serve(grpc_bind_address().parse().unwrap())
            .await
        {
            Ok(_) => {}
            Err(err) => error!("grpc server error: {:?}", err),
        };

        Ok(())
    });

    vec![http_server, grpc_server]
}
