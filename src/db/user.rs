use std::{convert::TryFrom, sync::Arc};

use async_mutex::Mutex;
use deadpool_postgres::{Object, Pool, PoolError};
use uuid::Uuid;

use super::DbError;
use crate::model::User;

const FIND_BY_USERNAME: &str =
    "select id, username, full_name, avatar_url, joined_at, primary_email, two_fa_secret, is_2fa_enabled from users where username = $1";
const FIND_BY_ID: &str =
    "select id, username, full_name, avatar_url, joined_at, primary_email, two_fa_secret, is_2fa_enabled from users where id = $1";
const FIND_BY_IDS: &str =
    "select users.id, username, full_name, avatar_url, joined_at, primary_email, two_fa_secret, is_2fa_enabled from users inner join (select ids as id, row_number() over () as row_num from unnest($1::uuid[]) as ids) as ids on ids.id = users.id order by row_num";
const UPDATE_USER_ACCOUNT_DATA: &str =
    "update users set username = $2, full_name = $3, avatar_url = $4 where id = $1";

pub struct UserRepository {
    pool: Arc<Mutex<Pool>>,
}

impl UserRepository {
    pub fn new(pool: Arc<Mutex<Pool>>) -> Self {
        Self { pool }
    }

    async fn get_connection(&self) -> Result<Object, PoolError> {
        self.pool.lock().await.get().await
    }

    pub async fn find_by_username(&self, username: &str) -> Result<Option<User>, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(FIND_BY_USERNAME).await?;

        match client.query(&smt, &[&username]).await?.into_iter().next() {
            Some(row) => Ok(Some(User::try_from(row)?)),
            None => Ok(None),
        }
    }

    pub async fn find_by_id(&self, id: &Uuid) -> Result<Option<User>, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(FIND_BY_ID).await?;

        match client.query(&smt, &[&id]).await?.into_iter().next() {
            Some(row) => Ok(Some(User::try_from(row)?)),
            None => Ok(None),
        }
    }

    pub async fn find_by_ids(&self, ids: &[Uuid]) -> Result<Vec<User>, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(FIND_BY_IDS).await?;

        let rows = client.query(&smt, &[&ids]).await?;

        let mut users = Vec::with_capacity(rows.len());
        for raw_user in rows {
            users.push(User::try_from(raw_user)?);
        }

        Ok(users)
    }

    pub async fn update_user_account_data(&self, user: &User) -> Result<(), DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(UPDATE_USER_ACCOUNT_DATA).await?;

        client
            .execute(
                &smt,
                &[&user.id, &user.username, &user.full_name, &user.avatar_url],
            )
            .await?;

        Ok(())
    }
}
