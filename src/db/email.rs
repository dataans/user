use std::{convert::TryFrom, sync::Arc};

use async_mutex::Mutex;
use deadpool_postgres::{Object, Pool, PoolError};
use uuid::Uuid;

use super::DbError;
use crate::model::Email;

const FIND_USER_EMAILS: &str = "select id, email, user_id, added_at from emails where user_id = $1";
const FIND_BY_EMAIL: &str = "select id, email, user_id, added_at from emails where email = $1";

pub struct EmailRepository {
    pool: Arc<Mutex<Pool>>,
}

impl EmailRepository {
    pub fn new(pool: Arc<Mutex<Pool>>) -> Self {
        Self { pool }
    }

    async fn get_connection(&self) -> Result<Object, PoolError> {
        self.pool.lock().await.get().await
    }

    pub async fn find_by_email(&self, email: &str) -> Result<Option<Email>, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(FIND_BY_EMAIL).await?;

        match client.query(&smt, &[&email]).await?.into_iter().next() {
            Some(row) => Ok(Some(Email::try_from(row)?)),
            None => Ok(None),
        }
    }

    pub async fn find_user_emails(&self, user_id: &Uuid) -> Result<Vec<Email>, DbError> {
        let client = self.get_connection().await?;
        let smt = client.prepare_cached(FIND_USER_EMAILS).await?;

        let rows = client.query(&smt, &[&user_id]).await?;

        let mut emails = Vec::with_capacity(rows.len());
        for raw_user in rows {
            emails.push(Email::try_from(raw_user)?);
        }
        Ok(emails)
    }
}
