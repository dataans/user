#[actix_web::main]
async fn main() -> std::io::Result<()> {
    for server in user::app::start_app().await {
        server.await.unwrap().unwrap();
    }

    Ok(())
}
