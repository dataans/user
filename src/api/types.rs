use actix_web::{http::StatusCode, HttpRequest, HttpResponse, Responder};
use serde::{Deserialize, Serialize};
use time::{serde::rfc3339, OffsetDateTime};
use uuid::Uuid;

use crate::model::Email;

#[derive(Serialize, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct UserResponse {
    pub id: Uuid,
    pub username: String,
    pub full_name: String,
    pub emails: Vec<String>,
    pub primary_email: String,
    pub avatar_url: String,
    #[serde(with = "rfc3339")]
    pub joined_at: OffsetDateTime,
    pub is_2fa_enabled: bool,
}

impl Responder for UserResponse {
    type Body = String;

    fn respond_to(self, _req: &HttpRequest) -> HttpResponse<Self::Body> {
        HttpResponse::<String>::with_body(StatusCode::OK, serde_json::to_string(&self).unwrap())
    }
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct UpdateUserAccountDataRequest {
    pub username: Option<String>,
    pub full_name: Option<String>,
    pub avatar_url: Option<String>,
}

#[derive(Serialize, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct EmailResponse {
    pub id: Uuid,
    pub email: String,
    #[serde(with = "rfc3339")]
    pub added_at: OffsetDateTime,
}

impl From<Email> for EmailResponse {
    fn from(email: Email) -> Self {
        let Email {
            id,
            email,
            added_at,
            ..
        } = email;
        Self {
            id,
            email,
            added_at,
        }
    }
}

#[derive(Serialize, Deserialize, Clone)]
pub struct RemoveEmailRequest {
    pub email: String,
}
