use actix_multipart::Multipart;
// use actix_multipart::Multipart;
use actix_web::{
    http::StatusCode,
    {get, post, put, web, HttpRequest}, {HttpResponse, Responder},
};
use futures_util::TryStreamExt;

use crate::{api::types::UpdateUserAccountDataRequest, app::AppData, services::user::UserError};

const AUTH_COOKIE_NAME: &str = "SessionId";

pub async fn default_route() -> impl Responder {
    HttpResponse::Ok().status(StatusCode::NOT_FOUND).body(())
}

#[get("/")]
pub async fn root_health() -> impl Responder {
    HttpResponse::Ok().body("root: ok.")
}

#[get("/api/v1/user/health")]
pub async fn health() -> impl Responder {
    HttpResponse::Ok().body("user ok.")
}

#[get("")]
pub async fn user_data(app: web::Data<AppData>, req: HttpRequest) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| UserError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.user_service
        .lock()
        .await
        .user_data(&session.user_id)
        .await
}

#[post("/avatar")]
pub async fn upload_image(
    mut payload: Multipart,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| UserError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    let avatar_file = payload
        .try_next()
        .await?
        .ok_or_else(|| UserError::Form("missing file field".into()))?;

    app.user_service
        .lock()
        .await
        .change_avatar(avatar_file, &session.user_id)
        .await
}

#[put("")]
pub async fn update_user_account_data(
    data: web::Json<UpdateUserAccountDataRequest>,
    app: web::Data<AppData>,
    req: HttpRequest,
) -> impl Responder {
    let session_cookie = req
        .cookie(AUTH_COOKIE_NAME)
        .ok_or_else(|| UserError::NotAuthorized("is not authorized".into()))?;
    let session = app
        .session_service
        .lock()
        .await
        .get_session(session_cookie.value())
        .await?;

    app.user_service
        .lock()
        .await
        .update_user_account_data(data.into_inner(), &session.user_id)
        .await
}
