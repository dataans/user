use std::{str::FromStr, sync::Arc};

use prost_types::Timestamp;
use thiserror::Error;
use tonic::{Request, Response, Status};
use uuid::Uuid;

use crate::{
    api::types::UserResponse,
    generated::user::{user_service_server::UserService as GrpcUserService, User, Users, UsersIds},
    services::user::{UserError, UserService},
};

#[derive(Error, Debug)]
pub enum GrpcError {
    #[error("uuid parse error: {0:?}")]
    Uuid(#[from] uuid::Error),
    #[error("unsupported: {0}")]
    Unsupported(String),
    #[error("Missing: {0}")]
    Missing(String),
}

impl From<GrpcError> for Status {
    fn from(error: GrpcError) -> Self {
        Status::invalid_argument(error.to_string())
    }
}

impl From<UserError> for Status {
    fn from(err: UserError) -> Self {
        match err {
            UserError::NotAuthorized(err) => Status::unauthenticated(err),
            err => Status::internal(err.to_string()),
        }
    }
}

impl From<UserResponse> for User {
    fn from(user: UserResponse) -> Self {
        let UserResponse {
            id,
            username,
            full_name,
            avatar_url,
            joined_at,
            primary_email,
            ..
        } = user;

        User {
            id: id.to_string(),
            username,
            primary_email,
            full_name,
            avatar_url,
            joined_at: Some(Timestamp {
                seconds: joined_at.unix_timestamp(),
                nanos: 0,
            }),
        }
    }
}

pub struct UserGrpcServer {
    user_service: Arc<UserService>,
}

impl UserGrpcServer {
    pub fn new(user_service: Arc<UserService>) -> Self {
        Self { user_service }
    }
}

#[tonic::async_trait]
impl GrpcUserService for UserGrpcServer {
    async fn get_users(&self, request: Request<UsersIds>) -> Result<Response<Users>, Status> {
        let mut ids = Vec::with_capacity(request.get_ref().ids.len());
        for id in request.into_inner().ids {
            ids.push(Uuid::from_str(&id).map_err(GrpcError::Uuid)?);
        }

        Ok(Response::new(Users {
            users: self
                .user_service
                .users_data(&ids)
                .await?
                .into_iter()
                .map(|user| user.into())
                .collect(),
        }))
    }
}
