use std::convert::TryFrom;

use deadpool_postgres::tokio_postgres::Row;
use time::OffsetDateTime;
use uuid::Uuid;

pub struct User {
    pub id: Uuid,
    pub username: String,
    pub full_name: String,
    pub avatar_url: String,
    pub joined_at: OffsetDateTime,
    pub primary_email: String,
    pub two_fa_secret: Option<String>,
    pub is_2fa_enabled: bool,
}

impl TryFrom<Row> for User {
    type Error = postgres::Error;

    fn try_from(row: Row) -> Result<Self, Self::Error> {
        Ok(Self {
            id: row.try_get(0)?,
            username: row.try_get(1)?,
            full_name: row.try_get(2)?,
            avatar_url: row.try_get(3)?,
            joined_at: row.try_get(4)?,
            primary_email: row.try_get(5)?,
            two_fa_secret: row.try_get(6)?,
            is_2fa_enabled: row.try_get(7)?,
        })
    }
}

pub struct Email {
    pub id: Uuid,
    pub email: String,
    pub user_id: Uuid,
    pub added_at: OffsetDateTime,
}

impl TryFrom<Row> for Email {
    type Error = postgres::Error;

    fn try_from(row: Row) -> Result<Self, Self::Error> {
        Ok(Self {
            id: row.try_get(0)?,
            email: row.try_get(1)?,
            user_id: row.try_get(2)?,
            added_at: row.try_get(3)?,
        })
    }
}
